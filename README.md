## Configuration

* Set own thingspeak api key for saving data (apiKey)
* Set own wifi connection - id and password (wifiSsid, wifiPassword)


## Problems

**White screen on display**

In file User_Setup.h (library TFT_eSPI) check and set proper pins of ESP32 for connection with display:

```
#define TFT_MOSI 23

#define TFT_SCLK 18

#define TFT_CS   15

#define TFT_DC    2

#define TFT_RST   4
```