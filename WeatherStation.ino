#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <HTTPClient.h>
#include <NTPClient.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include "SdsDustSensor.h"
#include <SPI.h>
#include <TFT_eSPI.h>
#include "Adafruit_SGP30.h"
#include <RunningMedian.h>
#include "time.h"
#include "clear_day.c"
#include "clear_night.c"
#include "cloudy.c"
#include "partly_cloudy_day.c"
#include "partly_cloudy_night.c"
#include "lightRain.c"
#include "rain.c"
#include "unknown.c"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "us.pool.ntp.org", 3600);
Adafruit_BME280 bme;
Adafruit_SGP30 sgp;
HardwareSerial& serialSDS(Serial2);
SdsDustSensor sds(serialSDS);
TFT_eSPI tft = TFT_eSPI();

//SDS variables
const int PIN_SDS_RX = 16;
const int PIN_SDS_TX = 17;
// how often should be measured (60000ms * 3 = 180000 3min)
const int SDS_MEASUREMENT_INTERVAL = 180000;
// time before reading the sensor (10s)
const int SDS_WARMUP_TIME = 10000;
unsigned long sdsLastMeasurmentTime = 0;
bool isSdsWarming = false;
bool isSdsRunning = false;

//WIFI, API variables
const char* apiServerName = "https://api.thingspeak.com/update";
String apiKey = "***"; //api key for writing
const char *wifiSsid = "***";
const char *wifiPassword = "***";

//Time variables
const char* ntpServer = "us.pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 0;
struct tm dt;
int lastTmMin;

//config
const float epsilon = 0.001;
const bool DEBUG = 1;
unsigned long lastLoopTime = millis();
const int LOOP_INTERVAL = 1000;
bool alarm1 = false;
bool alarm2 = false;

//values types to display
const int TYPE_INT = 0;
const int TYPE_FLOAT = 1;

//data structures
struct SensorData {
    float median;
    float lastMedian;
    float valueRanges[2][2];
    int valueFormat;
    bool isError;   
    RunningMedian filter;
};

struct AirData {
    SensorData temperature;
    SensorData humidity;
    SensorData pressure;
    SensorData tvoc;
    SensorData eco2;
    SensorData pm25;
    SensorData pm10;
};

struct Error {
    bool wifi;
    bool time;
    bool sds;
    bool sgp;
    bool bme;
};

AirData airData = {
    {0, 0, {{19, 23},     {17, 25}},    TYPE_FLOAT, false, RunningMedian(5)},
    {0, 0, {{40, 60},     {30, 70}},    TYPE_INT,   false, RunningMedian(5)},
    {0, 0, {{1000, 1025}, {990, 1035}}, TYPE_FLOAT, false, RunningMedian(5)},
    {0, 0, {{0, 20},      {0, 100}},    TYPE_INT,   false, RunningMedian(5)},
    {0, 0, {{400, 1000},  {400, 2000}}, TYPE_INT,   false, RunningMedian(5)},
    {0, 0, {{0, 50},      {0, 100}},    TYPE_INT,   false, RunningMedian(5)},
    {0, 0, {{0, 50},      {0, 100}},    TYPE_INT,   false, RunningMedian(5)}
};

Error error = {
    true, true, true, true, true
};

void printLocalTime()
{
    if (!getLocalTime(&dt)) {
        Serial.println(F("Failed to obtain time"));
        error.time = true;
        return;
    }
    Serial.println(&dt, "%A, %B %d %Y %H:%M:%S");
}

void initBme()
{
    bool status;
    status = bme.begin(0x76);  
    if (!status) {
        if (DEBUG) Serial.println(F("Could not find a valid BME280 sensor, check wiring!"));
        error.bme = true;
    }
}

void initSds()
{
    serialSDS.begin(9600, SERIAL_8N1, PIN_SDS_RX, PIN_SDS_TX);
    delay(100);
    sdsLastMeasurmentTime = millis() - SDS_MEASUREMENT_INTERVAL;
    WorkingStateResult state = sds.sleep();

    if (state.isWorking()) {
        if (DEBUG) Serial.println(F("SDS - Problem with sleeping the sensor."));
        error.sds = true;
    } else {
        if (DEBUG) Serial.println(F("SDS - Sensor is sleeping"));
        error.sds = false;
    }
}

void initSgp()
{
    if (!sgp.begin()) {
        if (DEBUG) Serial.println(F("Sensor SGP30 not found"));
        error.sgp = true;
    }
    if (DEBUG) Serial.println(F("Found sensor SGP30"));
    error.sgp = false;
}

void createNumberField(const char *label, int labelX, int labelY, int valueX, int valueY, SensorData *measurement)
{
    tft.setTextColor(TFT_PINK);
    tft.drawString(label, labelX, labelY, 2);

    if (measurement->isError) {
        tft.setTextColor(TFT_RED);
        tft.drawString("E", valueX, valueY, 4);
    } else {
        if (measurement->median >= measurement->valueRanges[0][0] && measurement->median <= measurement->valueRanges[0][1]) {
            tft.setTextColor(TFT_GREEN);
        } else if (measurement->median >= measurement->valueRanges[1][0] && measurement->median <= measurement->valueRanges[1][1]) {
            tft.setTextColor(TFT_YELLOW);
        } else {
            tft.setTextColor(TFT_RED);
        }

        if (measurement->median > measurement->lastMedian) {
            tft.fillTriangle(valueX - 12, valueY + 10, valueX - 8, valueY + 4, valueX - 4, valueY + 10, TFT_SKYBLUE);
        } else if (measurement->median < measurement->lastMedian) {
            tft.fillTriangle(valueX - 12, valueY + 12, valueX - 8, valueY + 18, valueX - 4, valueY + 12, TFT_SKYBLUE);
        }
        measurement->lastMedian = measurement->median;
        
        if (measurement->valueFormat == TYPE_INT) {
            tft.drawNumber(measurement->median, valueX, valueY, 4);
        } else {
            tft.drawFloat(measurement->median, 1, valueX, valueY, 4);
        }
    }
}

void updateDisplay()
{
    if (lastTmMin == dt.tm_min
        && abs(airData.temperature.median - airData.temperature.lastMedian) < epsilon
        && abs(airData.humidity.median - airData.humidity.lastMedian) < epsilon
        && abs(airData.pressure.median - airData.pressure.lastMedian) < epsilon
        && abs(airData.pm25.median - airData.pm25.lastMedian) < epsilon
        && abs(airData.pm10.median - airData.pm10.lastMedian) < epsilon
        && abs(airData.eco2.median - airData.eco2.lastMedian) < epsilon
        && abs(airData.tvoc.median - airData.tvoc.lastMedian) < epsilon
        && error.wifi == false
        && error.time == false
        && error.sds == false
        && error.sgp == false
        && error.bme == false
    ) {
        return;
    }

    if (DEBUG) {
        Serial.print(F("TVOC = "));
        Serial.println(airData.tvoc.median);
    }

    tft.fillScreen(TFT_BLACK);
    tft.setSwapBytes(true);

    if (airData.pressure.isError || error.bme) {
        tft.pushImage(25, 20, 50, 50, unknownIcon);
    } else if (dt.tm_hour > 6 && dt.tm_hour < 20) {
        if (airData.pressure.median > 1025) {
            tft.pushImage(25, 20, 50, 50, clearDayIcon);
        } else if (airData.pressure.median > 1015) {
            tft.pushImage(25, 20, 50, 50, partlyCloudyDayIcon);
        } else if (airData.pressure.median > 1005) {
            tft.pushImage(25, 20, 50, 50, cloudyIcon);
        } else if (airData.pressure.median > 995) {
            tft.pushImage(25, 20, 50, 50, lightRainIcon);
        } else {
            tft.pushImage(25, 20, 50, 50, rainIcon);
        }
    } else {
        if (airData.pressure.median > 1025) {
            tft.pushImage(25, 20, 50, 50, clearNightIcon);
        } else if (airData.pressure.median > 1014) {
            tft.pushImage(25, 20, 50, 50, partlyCloudyNightIcon);
        }  else if (airData.pressure.median > 1005) {
            tft.pushImage(25, 20, 50, 50, cloudyIcon);
        } else if (airData.pressure.median > 995) {
            tft.pushImage(25, 20, 50, 50, lightRainIcon);
        } else {
            tft.pushImage(25, 20, 50, 50, rainIcon);
        }
    }

    //Time
    char buf[10];
    if (error.time) {
        snprintf(buf, sizeof(buf), "--:--");
    } else {
        snprintf(buf, sizeof(buf), "%d:%02d", dt.tm_hour, dt.tm_min);
    }
    tft.setCursor(tft.width()/3 + 35, 25, 7);
    tft.setTextColor(TFT_LIGHTGREY);
    tft.println(buf);

    int16_t w13 = tft.width()/3;
    int16_t w23 = 2 * tft.width()/3;
    int16_t h13 = tft.height()/3;
    int16_t h23 = 2 * tft.height()/3;
    int16_t h12 = tft.height()/2;

    //Ikonki
    int labelX = w13 + 10;
    int labelY = h12 - 30;
    int frameX = labelX - 3;
    int frameY = labelY - 2;
    int offsetX = 42;
    tft.setTextColor(TFT_LIGHTGREY);
    tft.fillRoundRect(frameX, frameY, 32, 20, 4, error.wifi ? TFT_RED : TFT_DARKGREEN);
    tft.drawString("WIFI", labelX, labelY, 2);
    labelX = labelX + offsetX;
    frameX = frameX + offsetX;
    tft.fillRoundRect(frameX, frameY, 36, 20, 4, error.time ? TFT_RED : TFT_DARKGREEN);
    tft.drawString("TIME", labelX, labelY, 2);
    offsetX = 46;
    labelX = labelX + offsetX;
    frameX = frameX + offsetX;
    tft.fillRoundRect(frameX, frameY, 30, 20, 4, error.sds ? TFT_RED : TFT_DARKGREEN);
    tft.drawString("SDS", labelX, labelY, 2);
    offsetX = 43;
    labelX = labelX + offsetX;
    frameX = frameX + offsetX;
    tft.fillRoundRect(frameX, frameY, 28, 20, 4, error.sgp ? TFT_RED : TFT_DARKGREEN);
    tft.drawString("SGP", labelX, labelY, 2);
    offsetX = 42;
    labelX = labelX + offsetX;
    frameX = frameX + offsetX;
    tft.fillRoundRect(frameX, frameY, 32, 20, 4, error.bme ? TFT_RED : TFT_DARKGREEN);
    tft.drawString("BME", labelX, labelY, 2);
    
    //Temperature, humidity, pressure
    labelX = 4;
    int valueX = 20;
    tft.drawRoundRect(0, h13, w13 - 2, h23, 4, TFT_LIGHTGREY);
    tft.setTextDatum(TL_DATUM);
    createNumberField("T [`C]", labelX, h13 + 3, valueX, h13 + 24, &airData.temperature);
    createNumberField("H [%]", labelX, h13 + 55, valueX, h13 + 76, &airData.humidity);
    createNumberField("P [hPa]", labelX, h13 + 105, valueX, h13 + 126, &airData.pressure);

    //PM2.5, PM10
    labelX = w13 + 6;
    valueX = w13 + 25;
    tft.drawRoundRect(w13 + 2, h12, w13 - 2, h12, 4, TFT_LIGHTGREY);
    createNumberField("PM2.5 [ug/m3]", labelX, h12 + 3, valueX, h12 + 25, &airData.pm25);
    createNumberField("PM10 [ug/m3]", labelX, h12 + 55, valueX, h12 + 77, &airData.pm10);

    //eCO2, TVOC
    labelX = w23 + 6;
    valueX = w23 + 20;
    tft.drawRoundRect(2 * w13 + 2, h12, w13 - 2, h12, 4, TFT_LIGHTGREY);
    createNumberField("eCO2 [ppm]", labelX, h12 + 3, valueX, h12 + 25, &airData.eco2);
    createNumberField("TVOC [ppb]", labelX, h12 + 55, valueX, h12 + 77, &airData.tvoc);
}

void fillAirSensorData(SensorData *sensor, float value)
{
    if (value == NAN || (value > 0 && value > 10 * sensor->valueRanges[1][1])) {
        sensor->isError = true;
        return;
    }

    sensor->filter.add(value);
    sensor->median = sensor->filter.getMedian();
}

void updateAirData() {
    float  temp = NAN, hum = NAN, press = NAN, tvoc = NAN, eco2 = NAN;    
    temp = bme.readTemperature();
    hum = bme.readHumidity();
    press = bme.readPressure();
    error.bme = temp == NAN || hum == NAN || press == NAN;

    if (press != NAN) {
        press = press / 100.0F;
    }

    if (sgp.IAQmeasure()) {
        tvoc = sgp.TVOC;
        eco2 = sgp.eCO2;
        error.sgp = false;
    } else {
        error.sgp = true;
    }

    temp = temp - 4.3; //fix na grzanie w obudowie
    fillAirSensorData(&airData.temperature, temp);
    fillAirSensorData(&airData.humidity, hum);
    fillAirSensorData(&airData.pressure, press);
    fillAirSensorData(&airData.tvoc, tvoc);
    fillAirSensorData(&airData.eco2, eco2);
}

void updateSdsData() 
{
    if (!isSdsWarming && !isSdsRunning && (millis() - sdsLastMeasurmentTime >= SDS_MEASUREMENT_INTERVAL)) {
        if (DEBUG) Serial.println(F("Activate SDS to warmup"));
        isSdsWarming = true;
        sds.wakeup();
        return;
    }

    if (isSdsWarming && (millis() - sdsLastMeasurmentTime >= SDS_MEASUREMENT_INTERVAL + SDS_WARMUP_TIME)) {
        isSdsWarming = false;
        isSdsRunning = true;
    }

    if (isSdsRunning) {
        PmResult pm = sds.queryPm();
        if (pm.isOk()) {
            airData.pm25.isError = false;
            airData.pm10.isError = false;
            error.sds = false;
            if (DEBUG) Serial.println(F("SDS - pomiar"));
            fillAirSensorData(&airData.pm25, pm.pm25);
            fillAirSensorData(&airData.pm10, pm.pm10);
        } else {
            if (DEBUG) {
                Serial.print(F("SDS - Could not read values from sensor, reason: "));
                Serial.println(pm.statusToString());        
            }
            airData.pm25.isError = true;
            airData.pm10.isError = true;
            error.sds = true;
        }
    }

    if (airData.pm25.filter.isFull()) {
        if (DEBUG) Serial.println(F("SDS - All samples collected"));
        sdsLastMeasurmentTime = millis();
        isSdsRunning = false;
        airData.pm25.filter.clear();        
        airData.pm10.filter.clear();        
        WorkingStateResult state = sds.sleep();

        if (state.isWorking()) {
            if (DEBUG) Serial.println(F("SDS - Problem with sleeping the sensor."));
            error.sds = true;
        } else {
            if (DEBUG) Serial.println(F("SDS - Sensor is sleeping"));
            error.sds = false;
        }
    }
}

void sendApiRequest()
{
    if (WiFi.status() == WL_CONNECTED) {
        HTTPClient http;
        http.begin(apiServerName);
        http.addHeader("Content-Type", "application/x-www-form-urlencoded");
        String httpRequestData = "api_key=" + apiKey;

        if (!error.bme) {
            httpRequestData = httpRequestData
                + "&field1=" + String(airData.temperature.median)
                + "&field2=" + String(airData.humidity.median)
                + "&field3=" + String(airData.pressure.median);
        }

        if (!error.sds && airData.pm25.median > epsilon && airData.pm10.median > epsilon) {
            httpRequestData = httpRequestData
                + "&field4=" + String(airData.pm25.median)
                + "&field5=" + String(airData.pm10.median);
        }

        if (!error.sgp) {
             httpRequestData = httpRequestData
                 + "&field6=" + String(airData.eco2.median)
                + "&field7=" + String(airData.tvoc.median);
        }

        int httpResponseCode = http.POST(httpRequestData);
        if (DEBUG) {
            Serial.print(F("HTTP Response code: "));
            Serial.println(httpResponseCode);    
        }
        http.end();
    } else {
        if (DEBUG) Serial.println(F("WiFi Disconnected"));
        error.wifi = true;
    }
}

void updateTime()
{
    if (!getLocalTime(&dt)){
        if (DEBUG) Serial.println("Failed to obtain time");
        return;
    }
}

void initSerial()
{
    Serial.begin(115200);
    delay(1000);
}

void initWifi()
{
    int connectTry = 0;
    error.wifi = false;
    WiFi.begin(wifiSsid, wifiPassword);

    if (DEBUG) Serial.println(F("Connecting"));

    while(WiFi.status() != WL_CONNECTED) {
        delay(500);
        if (DEBUG) Serial.print(".");
        if (connectTry > 4) {
            error.wifi = true;
            break;
        }
        connectTry++;
    }    

    if (DEBUG) {
        Serial.print(F("Connected to WiFi network with IP Address: "));
        Serial.println(WiFi.localIP());        
    }
}

void initDisplay()
{
    tft.init();
    tft.setRotation(3);
    tft.fillScreen(TFT_BLACK);
    tft.setTextDatum(TL_DATUM);
    tft.setTextColor(TFT_GOLD);
    tft.drawString("Stacja pogody", 66, 60, 4);
    tft.setTextColor(TFT_SILVER);
    tft.drawString("moja-elka.blogspot.com", 80, 120, 2);

    if (DEBUG) {
        Serial.println(F("initDisplay DONE"));
    }
}

void initClock()
{
    timeClient.begin();
    delay(1000);
    if (timeClient.update()) {
        error.time = false;
    } else {
        error.time = true;
    }
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    if (DEBUG) printLocalTime();
}

void setup()
{
    initSerial();
    initDisplay();
    initSds();
    initBme();
    initSgp();
    initWifi();
    initClock();
    updateAirData();
}

void loop() 
{
    if (millis() - lastLoopTime > LOOP_INTERVAL) {
        updateTime();
        updateSdsData();
        lastLoopTime = millis();
    }

    if ((dt.tm_sec % 5) == 0) {
        if (alarm1 == false) {
            updateAirData();
            updateDisplay();
            lastTmMin = dt.tm_min;
            alarm1 = true;
        }
    } else {
        alarm1 = false;
    }

    if ((dt.tm_min % 5) == 0) {
        if (alarm2 == false) {
            sendApiRequest();
            alarm2 = true;
        }
    } else {
        alarm2 = false;
    }

    delay(50);
}